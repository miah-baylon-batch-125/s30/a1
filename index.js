const express = require('express');

const mongoose = require('mongoose'); //this code is to be used on our db connection and to ctreare our schema and our model for our exissting MongoDB atlas collection

const app = express(); // creating a server through the use of app 
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

//mongoose.connect - is a way to connect our mongodb atlas db connection string to our server 

//paste inside the connect() method the connection string copied from the mongodb atlas db, it must be enclosed w/ double/single/backticks quote
//remember to replace the password and the database name with their actual values
//Due to updates made by MongoDB Atlas developers, the default connection is being flagged as an error, to skip that error or warning that we are going to encounter in the future, we will use the useNewUrlParser and useUnifiedTopology objects inside our mongoose.connect
mongoose.connect("mongodb+srv://ppr0119:admin@cluster0.pzreh.mongodb.net/b125-tasks?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology:true
	}
).then(()=> { // if the mongoose succedded on the connection, then we will console.log message
	console.log("Successfully Connected to Database!");
}).catch((error) => { //handles error when the mongoose failed to connect on our mongodb atlas database
	console.log(error);
});

/*---------------------------------------------------------------------*/
//Schema- gives a structure of what kind of record/connection we are going to contain on our database

//Schema() method - determines the structure of documents to be written in the database
//Schema acts as blueprint to our data
//we used the schema() constructor of the mongoose dependency to create a new schema object
//the "new" keyword, creates a new Schema

const taskSchema = new mongoose.Schema({
	//Define the fields with their corresponding data type
	//For task, it needs a field called 'name' and 'status' 
	//the field name has a data type of string
	//he field status has a data type of 'boolean' with the default value of 'false'
	name: String,
	status: {
		type: Boolean,
		//default values are predefined values for a field if we dont put any value
		default: false
	}
});

//to perform the CRUD operation for our defined collections with corresponding schema
//the Task variable will contain the model for our tasks collection that shall perform the CRUD operations
//the first parameter of the mongoose.model indicates the collection in where to store the data. take note: the collection must be written in singular form and the first letter of the name must in uppercase
//the second parameter is used to specify the schema/blueprint of he documents that will be stored on the tasks collection
const Task = mongoose.model('Task', taskSchema); 

const userSchema = new mongoose.Schema({
	firstName: String,
	lastName: String,
	userName: String,
	password: String
});


const User = mongoose.model('User', userSchema); 

/*
	Business Logic - Todo List Application
		-CRUD Operation for our Tasks collection

*/

//insert new task
app.post('/add-task', (req, res) => {
	//call model for our tasks collection
	//create an instance of the task model and save it to our database
	//creating a new task with a task name "PM break" through the use of the task model
	let newTask = new Task({
		name: req.body.name

	});
	//Telling our server that the NewTask will now be saved as a new document to our task collection on our database
	//.save() - saves a new document to our db
	//on our callback, it will receive 2 values, the error and the saved document
	//error value shall contain the error whenever there is an error encountered while we are saving our document
	//savedTask shall contain the newly saved document from the db once the saving process is successful
	newTask.save((error, savedTask) => {
		if (error) {
			console.log(error);
		} else {
			res.send(`New task saved! ${savedTask}`);
		}

	});
});


app.post('/register', (req, res) => {
	
	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		username: req.body.username,
		password: req.body.password


	});

	newUser.save((error, savedUser) => {
		if (error) {
			console.log(error);
		} else {
			res.send(`New user saved! ${savedUser}`);
		}

	});
});

//Retrieve
app.get('/retrieve-tasks', (req, res) => {
	//find({}) will retrieve all the documents from the tasks collection
	//the error on the callback will handle the errors encountered while retrieveing the records
	//the records on the callback will handle the raw data from the db
	Task.find({}, (error, records) => {
		if(error){
			console.log(error);
		} else {
			res.send(records);
		}
	})
});

//retrieve tasks that are done, means the status = true
app.get('/retrieve-tasks-done', (req, res) => {
	//the task model will return all the tasks that has a status equal to true
	Task.find({status: true}, (error, records) => {
		if(error){
			console.log(error);
		} else {
			res.send(records);
		}
	})
});

// Retrieve User
app.get('/retrieve-user', (req, res) => {
	User.find({}, (error, records) => {
		if(error){
			console.log(error);
		} else {
			res.send(records);
		}
	})
});

app.get('/retrieve-users-done', (req, res) => {
	User.find({status: true}, (error, records) => {
		if(error){
			console.log(error);
		} else {
			res.send(records);
		}
	})
});



//Update Operation

app.put('/complete-task/:taskId', (req, res) => {
	//res.send({ urlParams: req.params.taskId});

	//1. Find the specific record using its ID
	//2. Update it

	//findByIdAndUpdate(<id>,<options> <callback>)

	let taskId = req.params.taskId;
	// url parameter -these are the values defined on the URL
	//to get the url prameters - req.params.<paramsName>
	//:taskId is a way to indicate that we are going to receive a url parameter, these are what we called a wildcard
	Task.findByIdAndUpdate(taskId, {status: true}, (error, updatedTask) => {
			if (error){
				console.log(error)
			} else {
				res.send(`Task complete Successfully!`);
			}
	});
})

//User Update
app.put('/new-user/:userId', (req, res) => {
	let userId = req.params.userId;
	User.findByIdAndUpdate(userId, {status: true}, (error, updatedUser) => {
			if (error){
				console.log(error)
			} else {
				res.send(`Change User Successfully!`);
			}
	});
})




//Delete Operation

app.delete('/delete-task/:taskId', (req, res) => {
	//findByIdAndDelete() - finds the specific record using its ID and delete
	
	let taskId = req.params.taskId;
	Task.findByIdAndDelete(taskId, (error, deletedTask) => {
		if(error){
			console.log(error)
		} else {
			res.send('Task deleted!');
		}
	})
})

//Delete User

app.delete('/delete-user/:userId', (req, res) => {
	let userId = req.params.userId;
	User.findByIdAndDelete(userId, (error, deletedUser) => {
		if(error){
			console.log(error)
		} else {
			res.send('User deleted!');
		}
	})
})




app.listen(port, () => console.log (`Server is running at port ${port}`));

